<?php

namespace Tests\Feature;

use App\Course;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Class CourseRouteTest
 *
 * @package Tests\Feature
 */
class CourseRouteTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Assert that the course creation page can be accessed when logged in.
     */
    public function testAccessCreatePageWhenAuthenticated()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->get(route('courses.create'));
        $this->assertTrue($response->isOk());
    }

    /**
     * Assert that the course edit page can be accessed when logged in.
     */
    public function testAccessEditPageWhenAuthenticated()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();

        $response = $this->get(route('courses.edit', ['course' => $course->id]));
        $this->assertTrue($response->isOk());
    }

    /**
     * Assert that a course can be inserted with valid data.
     */
    public function testCreateCourseWithValidData()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday ? true : null,
            'tuesday' => $courseDetails->tuesday ? true : null,
            'wednesday' => $courseDetails->wednesday ? true : null,
            'thursday' => $courseDetails->thursday ? true : null,
            'friday' => $courseDetails->friday ? true : null,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a course can be inserted with only Monday selected.
     */
    public function testCreateCourseOnlyMondayIsValid()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => true,
            'tuesday' => null,
            'wednesday' => null,
            'thursday' => null,
            'friday' => null,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a course can be inserted with only Tuesday selected.
     */
    public function testCreateCourseOnlyTuesdayIsValid()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => null,
            'tuesday' => true,
            'wednesday' => null,
            'thursday' => null,
            'friday' => null,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a course can be inserted with only Wednesday selected.
     */
    public function testCreateCourseOnlyWednesdayIsValid()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => null,
            'tuesday' => null,
            'wednesday' => true,
            'thursday' => null,
            'friday' => null,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a course can be inserted with only Thursday selected.
     */
    public function testCreateCourseOnlyThursdayIsValid()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => null,
            'tuesday' => null,
            'wednesday' => null,
            'thursday' => true,
            'friday' => null,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a course can be inserted with only Friday selected.
     */
    public function testCreateCourseOnlyFridayIsValid()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => null,
            'tuesday' => null,
            'wednesday' => null,
            'thursday' => null,
            'friday' => true,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a course can be updated with valid data.
     */
    public function testUpdateCourseWithValidData()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => $newCourseDetails->end_date,
            'start_time' => $newCourseDetails->start_time,
            'end_time' => $newCourseDetails->end_time,
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday ? true : null,
            'tuesday' => $newCourseDetails->tuesday ? true : null,
            'wednesday' => $newCourseDetails->wednesday ? true : null,
            'thursday' => $newCourseDetails->thursday ? true : null,
            'friday' => $newCourseDetails->friday ? true : null,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        $fetchedCourse = Course::find($course->id);
        // NOTE: Sometimes the CRNs don't match but it doesn't happen consistently...
        // TODO: Find out why the CRNs don't match on some, but not all, test runs
        $this->assertEquals($newCourseDetails->crn, $fetchedCourse->crn);
        $this->assertEquals($newCourseDetails->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($newCourseDetails->course_number, $fetchedCourse->course_number);
        $this->assertEquals($newCourseDetails->section, $fetchedCourse->section);
        $this->assertEquals($newCourseDetails->title, $fetchedCourse->title);
        $this->assertEquals($newCourseDetails->start_date, $fetchedCourse->start_date);
        $this->assertEquals($newCourseDetails->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($newCourseDetails->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($newCourseDetails->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($newCourseDetails->timezone, $fetchedCourse->timezone);
        $this->assertEquals($newCourseDetails->monday, $fetchedCourse->monday);
        $this->assertEquals($newCourseDetails->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($newCourseDetails->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($newCourseDetails->thursday, $fetchedCourse->thursday);
        $this->assertEquals($newCourseDetails->friday, $fetchedCourse->friday);
        $this->assertEquals($newCourseDetails->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($user->id, $fetchedCourse->updated_by);
    }

    // Negative Page Authorization ---------------------------------------------

    /**
     * Assert that when the page is accessed as an unauthorized guest, it redirects the user.
     */
    public function testAccessCreatePageUnauthorized()
    {
        $response = $this->get(route('courses.create'));
        $response->assertRedirect();
    }

    /**
     * Assert that when the page is accessed as an unauthorized guest, it redirects the user.
     */
    public function testAccessUpdatePageUnauthorized()
    {
        $course = factory(Course::class)->create();

        $response = $this->get(route('courses.edit', ['course' => $course->id]));
        $response->assertRedirect();
    }

    // Negative Create Tests ---------------------------------------------------

    /**
     * Assert that submitting the CRN with an empty string fails validation.
     */
    public function testCreateCourseWithBlankCrn()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => '',
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the CRN with a non-numeric string fails validation.
     */
    public function testCreateCourseWithNonNumericCrn()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => 'Not a valid CRN',
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the subject code with an empty string fails validation.
     */
    public function testCreateCourseWithBlankSubjectCode()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => '',
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the course number with an empty string fails validation.
     */
    public function testCreateCourseWithBlankCourseNumber()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => '',
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the section with an empty string fails validation.
     */
    public function testCreateCourseWithBlankSection()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => '',
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the title with an empty string fails validation.
     */
    public function testCreateCourseWithBlankTitle()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => '',
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the start date with an empty string fails validation.
     */
    public function testCreateCourseWithBlankStartDate()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => '',
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the start date with a malformed date string fails validation.
     */
    public function testCreateCourseWithMalformedStartDate()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => '2-5-2018',
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the start date with an invalid date string fails validation.
     */
    public function testCreateCourseWithInvalidStartDate()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => 'Not a valid date',
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the end date with an empty string fails validation.
     */
    public function testCreateCourseWithBlankEndDate()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => '',
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the end date with a malformed date string fails validation.
     */
    public function testCreateCourseWithMalformedEndDate()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => '4-16-2018 12:45 AM',
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the start date with an invalid date string fails validation.
     */
    public function testCreateCourseWithInvalidEndDate()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => 'Not a valid date',
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the start date with a date later than the end date fails validation.
     */
    public function testCreateCourseWithStartDateLaterThanEndDate()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => '2018-01-15',
            'end_date' => '2018-01-12',
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the start time with an empty string fails validation.
     */
    public function testCreateCourseWithBlankStartTime()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => '',
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the start time with a malformed time string fails validation.
     */
    public function testCreateCourseWithMalformedStartTime()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => '5:36',
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the start time with an invalid time fails validation.
     */
    public function testCreateCourseWithInvalidStartTime()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => '25:00',
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the end time with a blank time fails validation.
     */
    public function testCreateCourseWithBlankEndTime()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => '',
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the end time with a blank time fails validation.
     */
    public function testCreateCourseWithMalformedEndTime()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => '0356',
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the end time with an invalid time fails validation.
     */
    public function testCreateCourseWithInvalidEndTime()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => '05:75',
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the start time with a time later than the end time fails validation.
     */
    public function testCreateCourseWithStartTimeLaterThanEndTime()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => '13:25',
            'end_time' => '11:00',
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the start time with a blank course term ID fails validation.
     */
    public function testCreateCourseWithBlankTimezone()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => '',
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the start time with a blank course term ID fails validation.
     */
    public function testCreateCourseWithInvalidTimezone()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => 'America/NotReallyEastern',
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that creating a course with no meeting days checked fails validation.
     */
    public function testCreateCourseWithNoMeetingDaysSelected()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => null,
            'tuesday' => null,
            'wednesday' => null,
            'thursday' => null,
            'friday' => null,
            'course_term_id' => $courseDetails->course_term_id,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that creating a course with the course term ID blank fails validation.
     */
    public function testCreateCourseWithBlankCourseTermId()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => '',
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that submitting the course term ID blank fails validation.
     */
    public function testCreateCourseWithInvalidCourseTermId()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => 'America/Eastern',
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => 0,
        ];

        $response = $this->post(route('courses.store'), $data);
        $response->assertRedirect();
    }

    // Negative Update Tests ---------------------------------------------------

    /**
     * Assert that submitting the CRN with an empty string fails validation.
     */
    public function testUpdateCourseWithBlankCrn()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => '',
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => $newCourseDetails->end_date,
            'start_time' => $newCourseDetails->start_time,
            'end_time' => $newCourseDetails->end_time,
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the CRN with a non-numeric string fails validation.
     */
    public function testUpdateCourseWithNonNumericCrn()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => 'Not a valid CRN',
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => $newCourseDetails->end_date,
            'start_time' => $newCourseDetails->start_time,
            'end_time' => $newCourseDetails->end_time,
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the subject code with an empty string fails validation.
     */
    public function testUpdateCourseWithBlankSubjectCode()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => '',
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => $newCourseDetails->end_date,
            'start_time' => $newCourseDetails->start_time,
            'end_time' => $newCourseDetails->end_time,
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the course number with an empty string fails validation.
     */
    public function testUpdateCourseWithBlankCourseNumber()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => '',
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => $newCourseDetails->end_date,
            'start_time' => $newCourseDetails->start_time,
            'end_time' => $newCourseDetails->end_time,
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the section with an empty string fails validation.
     */
    public function testUpdateCourseWithBlankSection()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => '',
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => $newCourseDetails->end_date,
            'start_time' => $newCourseDetails->start_time,
            'end_time' => $newCourseDetails->end_time,
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the title with an empty string fails validation.
     */
    public function testUpdateCourseWithBlankTitle()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => '',
            'start_date' => $newCourseDetails->start_date,
            'end_date' => $newCourseDetails->end_date,
            'start_time' => $newCourseDetails->start_time,
            'end_time' => $newCourseDetails->end_time,
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the start date with an empty string fails validation.
     */
    public function testUpdateCourseWithBlankStartDate()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => '',
            'end_date' => $newCourseDetails->end_date,
            'start_time' => $newCourseDetails->start_time,
            'end_time' => $newCourseDetails->end_time,
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the start date with a malformed date string fails validation.
     */
    public function testUpdateCourseWithMalformedStartDate()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => '2-5-2018',
            'end_date' => $newCourseDetails->end_date,
            'start_time' => $newCourseDetails->start_time,
            'end_time' => $newCourseDetails->end_time,
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the start date with an invalid date string fails validation.
     */
    public function testUpdateCourseWithInvalidStartDate()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => 'Not a valid date',
            'end_date' => $newCourseDetails->end_date,
            'start_time' => $newCourseDetails->start_time,
            'end_time' => $newCourseDetails->end_time,
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the end date with an empty string fails validation.
     */
    public function testUpdateCourseWithBlankEndDate()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => '',
            'start_time' => $newCourseDetails->start_time,
            'end_time' => $newCourseDetails->end_time,
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the end date with a malformed date string fails validation.
     */
    public function testUpdateCourseWithMalformedEndDate()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => '4-16-2018 12:45 AM',
            'start_time' => $newCourseDetails->start_time,
            'end_time' => $newCourseDetails->end_time,
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the start date with an invalid date string fails validation.
     */
    public function testUpdateCourseWithInvalidEndDate()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => 'Not a valid date',
            'start_time' => $newCourseDetails->start_time,
            'end_time' => $newCourseDetails->end_time,
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the start date with a date later than the end date fails validation.
     */
    public function testUpdateCourseWithStartDateLaterThanEndDate()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => '2018-01-15',
            'end_date' => '2018-01-12',
            'start_time' => $newCourseDetails->start_time,
            'end_time' => $newCourseDetails->end_time,
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the start time with an empty string fails validation.
     */
    public function testUpdateCourseWithBlankStartTime()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => $newCourseDetails->end_date,
            'start_time' => '',
            'end_time' => $newCourseDetails->end_time,
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the start time with a malformed time string fails validation.
     */
    public function testUpdateCourseWithMalformedStartTime()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => $newCourseDetails->end_date,
            'start_time' => '5:36',
            'end_time' => $newCourseDetails->end_time,
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the start time with an invalid time fails validation.
     */
    public function testUpdateCourseWithInvalidStartTime()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => $newCourseDetails->end_date,
            'start_time' => '25:00',
            'end_time' => $newCourseDetails->end_time,
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the end time with a blank time fails validation.
     */
    public function testUpdateCourseWithBlankEndTime()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => $newCourseDetails->end_date,
            'start_time' => $newCourseDetails->start_time,
            'end_time' => '',
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the end time with a blank time fails validation.
     */
    public function testUpdateCourseWithMalformedEndTime()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => $newCourseDetails->end_date,
            'start_time' => $newCourseDetails->start_time,
            'end_time' => '0356',
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the end time with an invalid time fails validation.
     */
    public function testUpdateCourseWithInvalidEndTime()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => $newCourseDetails->end_date,
            'start_time' => $newCourseDetails->start_time,
            'end_time' => '05:75',
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the start time with a time later than the end time fails validation.
     */
    public function testUpdateCourseWithStartTimeLaterThanEndTime()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => $newCourseDetails->end_date,
            'start_time' => '13:25',
            'end_time' => '11:00',
            'timezone' => $newCourseDetails->timezone,
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the start time with a blank course term ID fails validation.
     */
    public function testUpdateCourseWithBlankTimezone()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => $newCourseDetails->end_date,
            'start_time' => $newCourseDetails->start_time,
            'end_time' => $newCourseDetails->end_time,
            'timezone' => '',
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the start time with a blank course term ID fails validation.
     */
    public function testUpdateCourseWithInvalidTimezone()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => $newCourseDetails->end_date,
            'start_time' => $newCourseDetails->start_time,
            'end_time' => $newCourseDetails->end_time,
            'timezone' => 'America/NotReallyEastern',
            'monday' => $newCourseDetails->monday,
            'tuesday' => $newCourseDetails->tuesday,
            'wednesday' => $newCourseDetails->wednesday,
            'thursday' => $newCourseDetails->thursday,
            'friday' => $newCourseDetails->friday,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that creating a course with no meeting days checked fails validation.
     */
    public function testUpdateCourseWithNoMeetingDaysSelected()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $newCourseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $newCourseDetails->crn,
            'subject_code' => $newCourseDetails->subject_code,
            'course_number' => $newCourseDetails->course_number,
            'section' => $newCourseDetails->section,
            'title' => $newCourseDetails->title,
            'start_date' => $newCourseDetails->start_date,
            'end_date' => $newCourseDetails->end_date,
            'start_time' => $newCourseDetails->start_time,
            'end_time' => $newCourseDetails->end_time,
            'timezone' => $newCourseDetails->timezone,
            'monday' => null,
            'tuesday' => null,
            'wednesday' => null,
            'thursday' => null,
            'friday' => null,
            'course_term_id' => $newCourseDetails->course_term_id,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that creating a course with the course term ID blank fails validation.
     */
    public function testUpdateCourseWithBlankCourseTermId()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => $courseDetails->timezone,
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => '',
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }

    /**
     * Assert that submitting the course term ID blank fails validation.
     */
    public function testUpdateCourseWithInvalidCourseTermId()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $courseDetails = factory(Course::class)->make();

        $data = [
            'crn' => $courseDetails->crn,
            'subject_code' => $courseDetails->subject_code,
            'course_number' => $courseDetails->course_number,
            'section' => $courseDetails->section,
            'title' => $courseDetails->title,
            'start_date' => $courseDetails->start_date,
            'end_date' => $courseDetails->end_date,
            'start_time' => $courseDetails->start_time,
            'end_time' => $courseDetails->end_time,
            'timezone' => 'America/Eastern',
            'monday' => $courseDetails->monday,
            'tuesday' => $courseDetails->tuesday,
            'wednesday' => $courseDetails->wednesday,
            'thursday' => $courseDetails->thursday,
            'friday' => $courseDetails->friday,
            'course_term_id' => 0,
        ];

        $response = $this->patch(route('courses.update', ['course' => $course->id]), $data);
        $response->assertRedirect();

        // Assert that the record did not change.
        $fetchedCourse = Course::find($course->id);
        $this->assertEquals($course->crn, $fetchedCourse->crn);
        $this->assertEquals($course->subject_code, $fetchedCourse->subject_code);
        $this->assertEquals($course->course_number, $fetchedCourse->course_number);
        $this->assertEquals($course->section, $fetchedCourse->section);
        $this->assertEquals($course->title, $fetchedCourse->title);
        $this->assertEquals($course->start_date, $fetchedCourse->start_date);
        $this->assertEquals($course->end_date, $fetchedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $fetchedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $fetchedCourse->end_time);
        $this->assertEquals($course->timezone, $fetchedCourse->timezone);
        $this->assertEquals($course->monday, $fetchedCourse->monday);
        $this->assertEquals($course->tuesday, $fetchedCourse->tuesday);
        $this->assertEquals($course->wednesday, $fetchedCourse->wednesday);
        $this->assertEquals($course->thursday, $fetchedCourse->thursday);
        $this->assertEquals($course->friday, $fetchedCourse->friday);
        $this->assertEquals($course->course_term_id, $fetchedCourse->course_term_id);
        $this->assertEquals($course->created_by, $fetchedCourse->created_by);
        $this->assertEquals($course->updated_by, $fetchedCourse->updated_by);
    }
}
