<?php

namespace Tests\Unit;

use App\CourseTerm;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Class CourseTermTest
 *
 * @package Tests\Unit
 */
class CourseTermTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that an course term can be created and fetched properly.
     */
    public function testCreateCourseTerm()
    {
        $term = factory(CourseTerm::class)->create();

        $savedTerm = CourseTerm::find($term->id);

        $this->assertEquals($term->name, $savedTerm->name);
        $this->assertEquals($term->display_order, $savedTerm->display_order);
    }

    /**
     * Attempt to create two course terms with the same name.
     */
    public function testCreateTwoCourseTermWithSameName()
    {
        $term1 = factory(CourseTerm::class)->create();
        $this->expectException(\PDOException::class);
        factory(CourseTerm::class)->create([
            'name' => $term1->name,
        ]);
    }

    /**
     * Attempt to save two course terms with the same name but different casing.
     */
    public function testCreateTwoCourseTermsWithSameEmailDifferentCasingResultsIsAccepted()
    {
        // TODO: Find out why a PDOException isn't thrown in SQLite
        $this->markTestSkipped("PDOException should be thrown but isn't");

        $term1 = factory(CourseTerm::class)->create([
            'name' => 'Fall Time',
        ]);
        $this->expectException(\PDOException::class);
        factory(CourseTerm::class)->create([
            'name' => 'FALL TIME',
        ]);
    }

    /**
     * Attempt to create two course terms with similar-looking names.
     * One uses a Latin 'a' and the other uses a Cyrillic 'a'.
     */
    public function testCreateTwoCourseTermsWithSimilarEmailButWithDifferentUnicodeCharacters()
    {
        // Has "U+0061 LATIN SMALL LETTER A"
        $termWithLatin = factory(CourseTerm::class)->create([
            'name' => "F\u{0061}ll Term",
        ]);
        // Has "U+0430 CYRILLIC SMALL LETTER A"
        $termWithCyrillic = factory(CourseTerm::class)->create([
            'name' => "F\u{0430}ll Term",
        ]);

        $savedCourseTermWithLatin = CourseTerm::find($termWithLatin->id);
        $savedCourseTermWithCyrillic = CourseTerm::find($termWithCyrillic->id);

        // Assert that the database saved the Unicode character.
        $this->assertEquals($termWithLatin->name, $savedCourseTermWithLatin->name);
        $this->assertEquals($termWithCyrillic->name, $savedCourseTermWithCyrillic->name);

        // Assert that the ASCII 'a' is not equal to the Cyrillic small 'a'.
        $this->assertNotEquals('Fall Term', $savedCourseTermWithCyrillic->name);
    }

    /**
     * Attempt to create an course term with a display_order value that already exists.
     */
    public function testCreateCourseTermDisplayOrderWithSameNumber()
    {
        $term1 = factory(CourseTerm::class)->create();
        $this->expectException(\PDOException::class);
        factory(CourseTerm::class)->create([
            'display_order' => $term1->display_order,
        ]);
    }
}
