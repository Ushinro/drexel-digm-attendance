<?php

use Faker\Generator as Faker;

$factory->define(App\Student::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'university_id' => $faker->unique()->randomNumber(8),
        'email' => $faker->unique()->safeEmail,
        'active' => true,
    ];
});
