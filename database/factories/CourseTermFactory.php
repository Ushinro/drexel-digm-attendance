<?php

use Faker\Generator as Faker;

$factory->define(App\CourseTerm::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
        'display_order' => $faker->unique()->randomNumber(6, true),
    ];
});
