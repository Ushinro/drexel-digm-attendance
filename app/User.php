<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * @package App
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_admin' => 'boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'username',
        'password',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Get the roles associated with the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Get the courses associated with the user (instructor).
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function courses()
    {
        return $this->belongsToMany(Course::class);
    }

    /**
     * Get the course attendance records associated with the user (instructor).
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function courseAttendances()
    {
        return $this->hasMany(CourseAttendance::class);
    }

    /**
     * Get users that are active.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    /**
     * Get users that are inactive.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotActive($query)
    {
        return $query->where('active', false);
    }

    /**
     * Exclude the authenticated user.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotSelf($query)
    {
        return $query->where('id', '!=', auth()->user()->id);
    }

    /**
     * Set the user to inactive and save it to the database.
     *
     * @return bool
     */
    public function deactivate()
    {
        $this->active = false;

        return $this->save();
    }

    /**
     * Set the user to active and save it to the database.
     *
     * @return bool
     */
    public function reactivate()
    {
        $this->active = true;

        return $this->save();
    }

    /**
     * Check to see if the user has a particular role.
     *
     * @param $role
     *
     * @return bool
     */
    public function hasRole($role)
    {
        return $this->roles->contains('name', $role);
    }

    /**
     * Check to see if the user is an administrator user.
     *
     * @return bool
     */
    public function getIsAdminAttribute()
    {
        return $this->hasRole('Administrator');
    }

    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Return a human-readable Yes/No string for the active flag.
     *
     * @return string
     */
    public function getIsActiveReadableAttribute()
    {
        return $this->active ? 'Yes' : 'No';
    }

    /**
     * Get the user's full name and username formatted like so:
     *
     *  first_name last_name (username)
     *
     * Example:
     *  John Smith (john_smith)
     *
     * @return string
     */
    public function getFullNameAndUsernameAttribute()
    {
        return $this->full_name . ' (' . $this->username . ')';
    }
}
