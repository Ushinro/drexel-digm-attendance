<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AttendanceStatus
 * @package App
 */
class AttendanceStatus extends Model
{
    /**
     * Get the course attendances associated with the attendance status.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseAttendances()
    {
        return $this->hasMany(CourseAttendance::class);
    }

    /**
     * Get the Absent status from a collection.
     *
     * @return AttendanceStatus
     */
    public function absent()
    {
        return $this->where('name', 'Absent')->first();
    }

    /**
     * Get the Tardy status from a collection.
     *
     * @return AttendanceStatus
     */
    public function tardy()
    {
        return $this->where('name', 'Tardy')->first();
    }

    /**
     * Get the Present status from a collection.
     *
     * @return AttendanceStatus
     */
    public function present()
    {
        return $this->where('name', 'Present')->first();
    }
}
