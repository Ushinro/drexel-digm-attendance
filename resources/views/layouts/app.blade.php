<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'DIGM Attendance') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/reset.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app" class="app-grid">
        <header class="app-header">
            @include('layouts.nav')
        </header>

        <aside class="start sidebar">
            @yield('sidebar-left')
        </aside>

        <div class="center content">
            @yield('content')
        </div>

        <aside class="end sidebar">
            @yield('sidebar-right')
        </aside>

        <footer class="app-footer">
            @yield('footer')

            <section class="footer-info">© {{ \Carbon\Carbon::now()->year }}</section>
        </footer>
    </div>

    <!-- Scripts -->
    @yield('scripts')
</body>
</html>
