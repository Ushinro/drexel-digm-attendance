@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel-heading">Students</div>

        <div>
            @include('layouts.flash')

            <table class="table">
                <thead>
                    <tr class="table-header-row">
                        <th class="table-header-cell dash-student-first-name">First Name</th>
                        <th class="table-header-cell dash-student-last-name">Last Name</th>
                        <th class="table-header-cell dash-student-email">Email Address</th>
                        <th class="table-header-cell dash-student-active">Active</th>
                        <th class="table-header-cell dash-actions">
                            <a href="{{ route('students.create') }}" class="btn btn-primary btn-link">Add Student</a>
                        </th>
                    </tr>
                </thead>

                <tbody>
                    @if ($students->isNotEmpty())
                        @foreach ($students as $student)
                            <tr class="table-content-row">
                                <td class="table-content-cell dash-student-first-name">{{ $student->first_name }}</td>

                                <td class="table-content-cell dash-student-last-name">{{ $student->last_name }}</td>

                                <td class="table-content-cell dash-student-email">{{ $student->email }}</td>

                                <td class="table-content-cell dash-student-active">{{ $student->is_active_readable }}</td>

                                <td class="table-content-cell dash-actions">
                                    @if ($student->active)
                                        <form class="inline-form" method="POST" action="{{ route('students.deactivate', ['student' => $student->id]) }}">
                                            @method('DELETE')
                                            @csrf

                                            <button type="submit" class="btn btn-red btn-submit" onclick="window.confirm('Are you sure you want to remove {{ $student->full_name }}?');">Deactivate</button>
                                        </form>
                                    @else
                                        <form class="inline-form" method="POST" action="{{ route('students.reactivate', ['student' => $student->id]) }}">
                                            @csrf

                                            <button type="submit" class="btn btn-red btn-submit" onclick="window.confirm('Are you sure you want to activate {{ $student->full_name }}?');">Activate</button>
                                        </form>
                                    @endif

                                    <a href="{{ route('students.edit', ['student' => $student->id]) }}" class="btn btn-primary btn-link">Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr class="table-content-row">
                            <td class="table-content-cell" colspan="4">There are no students registered.</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
