@extends('layouts.app')

@section('content')
@php
    $isNewUser = !isset($user);
@endphp
<div class="container">
    <div class="panel-heading">
        @if ($isNewUser)
            Create User
        @else
            Edit User
        @endif
    </div>

    <div>
        @include('layouts.flash')

        <form id="edit-user-form"
              class="standard-form"
              method="POST"
              action="{{ route(
                  'users.' . ($isNewUser ? 'store' : 'update'),
                  $isNewUser ? [] : ['user' => $user->id]
              ) }}">
            @csrf

            @if (!$isNewUser)
                @method('PATCH')
            @endif

            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                <label for="first-name" class="control-label required">First Name</label>

                <div class="form-field">
                    <input type="text"
                           id="first-name"
                           class="form-control"
                           name="first_name"
                           value="{{ old('first_name', $isNewUser ? '' : $user->first_name) }}"
                           placeholder="John"
                           required
                           autofocus>

                    @if ($errors->has('first_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                <label for="last-name" class="control-label required">Last Name</label>

                <div class="form-field">
                    <input type="text"
                           id="last-name"
                           class="form-control"
                           name="last_name"
                           value="{{ old('last_name', $isNewUser ? '' : $user->last_name) }}"
                           placeholder="Smith"
                           required
                           autofocus>

                    @if ($errors->has('last_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="control-label required">E-Mail Address</label>

                <div class="form-field">
                    <input type="email"
                           id="email"
                           class="form-control"
                           name="email"
                           value="{{ old('email', $isNewUser ? '' : $user->email) }}"
                           placeholder="example@example.com"
                           required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                <label for="username" class="control-label required">Username</label>

                <div class="form-field">
                    <input type="text"
                           id="username"
                           class="form-control"
                           name="username"
                           value="{{ old('username', $isNewUser ? '' : $user->username) }}"
                           placeholder="abc123"
                           required>

                    @if ($errors->has('username'))
                        <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            @if ($isNewUser)
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="control-label">Password</label>

                    <div class="form-field">
                        <input type="password"
                               id="password"
                               class="form-control"
                               name="password">

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="control-label">Confirm Password</label>

                    <div class="form-field">
                        <input type="password"
                               id="password-confirm"
                               class="form-control"
                               name="password_confirmation">
                    </div>
                </div>
            @endif

            <div class="form-group{{ $errors->has('roles') ? ' has-error' : '' }}">
                <label for="role-id" class="control-label required">Role</label>

                <div class="form-field">
                    <select id="role-id"
                            class="form-control dropdown"
                            name="roles">
                        @foreach ($roles as $role)
                            <option value="{{ $role->id }}"
                                    @if (old('roles') == $role->id ||
                                        (!$isNewUser && $user->roles->contains('id', $role->id))) selected @endif>
                                {{ $role->name }}
                            </option>
                        @endforeach
                    </select>

                    @if ($errors->has('roles'))
                        <span class="help-block">
                            <strong>{{ $errors->first('roles') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </form>

        <div class="form-submit-buttons">
            <a class="btn btn-primary btn-link btn-submit" href="{{ route('users.index') }}">
                Cancel
            </a>

            <button type="submit" form="edit-user-form" class="btn btn-primary btn-submit">
                Update
            </button>
        </div>
    </div>
</div>
@endsection
