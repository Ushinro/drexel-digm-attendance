@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel-heading">
        {{ $user->full_name }}
    </div>

    <div class="course-grid">
        <table class="table course-details start">
            <tr class="table-content-row">
                <td class="table-content-cell course-detail-cell course-detail-label">Email:</td>
                <td class="table-content-cell course-detail-cell">{{ $user->email }}</td>
            </tr>

            <tr class="table-content-row">
                <td class="table-content-cell course-detail-cell course-detail-label">Username:</td>
                <td class="table-content-cell course-detail-cell">{{ $user->username }}</td>
            </tr>

            <tr class="table-content-row">
                <td class="table-content-cell course-detail-cell course-detail-label">Active:</td>
                <td class="table-content-cell course-detail-cell">{{ $user->is_active_readable }}</td>
            </tr>
        </table>

        <div class="start">
            <a href="{{ route('users.edit', ['user' => $user->id]) }}" class="btn btn-primary btn-link">Edit User</a>
        </div>
    </div>

    <table class="table dash-course-table">
        <caption class="table-title">Current Classes</caption>

        <thead>
            <tr class="table-header-row">
                <th class="table-header-cell dash-course-title">Course Title</th>
                <th class="table-header-cell dash-meeting-days">Meeting Days</th>
                <th class="table-header-cell dash-start-date">Start Date</th>
                <th class="table-header-cell dash-end-date">End Date</th>
                <th class="table-header-cell dash-start-time">Start Time</th>
                <th class="table-header-cell dash-end-time">End Time</th>
            </tr>
        </thead>

        <tbody>
            @if ($user->courses->isNotEmpty())
                @foreach ($user->courses as $course)
                    <tr class="table-content-row">
                        <td class="table-content-cell dash-course-title">
                            <abbr class="abbr-tooltip" title="{{ $course->title }}">
                                <p>{{ $course->course_code }}</p>
                                <p>{{ str_limit($course->title, $limit = 40, $end = '...') }}</p>
                            </abbr>
                        </td>

                        <td class="table-content-cell dash-meeting-days">
                            <ul>
                                @if ($course->monday)
                                    <li>Monday</li>
                                @endif
                                @if ($course->tuesday)
                                    <li>Tuesday</li>
                                @endif
                                @if ($course->wednesday)
                                    <li>Wednesday</li>
                                @endif
                                @if ($course->thursday)
                                    <li>Thursday</li>
                                @endif
                                @if ($course->friday)
                                    <li>Friday</li>
                                @endif
                            </ul>
                        </td>

                        <td class="table-content-cell dash-start-date">{{ $course->start_date }}</td>

                        <td class="table-content-cell dash-end-date">{{ $course->end_date }}</td>

                        <td class="table-content-cell dash-start-time">{{ $course->start_time_12 }}</td>

                        <td class="table-content-cell dash-end-time">{{ $course->end_time_12 }}</td>
                    </tr>
                @endforeach
            @else
                <tr class="table-content-row">
                    <td class="table-content-cell" colspan="6">This instructor is has no classes at this time.</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
@endsection
